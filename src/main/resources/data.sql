insert into "shareholder" ("first_name", "last_name") values ('Dima', 'Cherniavskiy');
insert into "shareholder" ("first_name", "last_name") values ('Aleksey', 'Obuhov');
insert into "shareholder" ("first_name", "last_name") values ('Maksim', 'Smirnov');

insert into "stock" ("name", "start", "current", "shareholder") values ('Sber', 1, 2, 1);
insert into "stock" ("name", "start", "current", "shareholder") values ('Tinkoff', 1, 3, 2);
insert into "stock" ("name", "start", "current", "shareholder") values ('Tesla', 2, 4, 2);
insert into "stock" ("name", "start", "current", "shareholder") values ('Gazprom', 2, 5,2);
insert into "stock" ("name", "start", "current", "shareholder") values ('American Airlines', 3, 6, 3);

