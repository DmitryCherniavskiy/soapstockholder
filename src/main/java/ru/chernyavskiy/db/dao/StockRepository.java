package ru.chernyavskiy.db.dao;

import ru.chernyavskiy.db.model.Stock;
import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;

public interface StockRepository extends CrudRepository<Stock, Integer> {

     List<Stock> findByShareholder(Integer shareholder);

     Optional<Stock> findById(Integer id);

     void deleteByShareholder(Integer shareholderId);
}
