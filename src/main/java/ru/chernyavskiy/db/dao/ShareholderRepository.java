package ru.chernyavskiy.db.dao;

import ru.chernyavskiy.db.model.Shareholder;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface ShareholderRepository extends CrudRepository<Shareholder, Integer> {

    Optional<Shareholder> findById(Integer id);
}