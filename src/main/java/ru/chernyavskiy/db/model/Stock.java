package ru.chernyavskiy.db.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "stock")
public class Stock
{
    public Stock(String sName, Double sStart, Double sCurrent, int sShareholder)
    {
        name = sName;
        start = sStart;
        current = sCurrent;
        shareholder = sShareholder;
    }

    public Stock() {

    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Integer id;

    @Column(name = "name")
    private String name;

    @Column(name = "start")
    private Double start;

    @Column(name = "current")
    private Double current;

    @Column(name = "shareholder")
    private int shareholder;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public void setName(String name) {this.name = name;}

    public void setStart(Double start) {
        this.start = start;
    }

    public void setCurrent(Double current) {
        this.current = current;
    }

    public void setShareholder(int shareholder) {
        this.shareholder = shareholder;
    }

    public String getName() {
        return name;
    }

    public Double getStart()
    {
        return start;
    }

    public Double getCurrent()
    {
        return current;
    }

    public int getShareholder()
    {
        return shareholder;
    }
}
