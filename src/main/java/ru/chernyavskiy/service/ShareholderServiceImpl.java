package ru.chernyavskiy.service;


import ru.chernyavskiy.db.dao.ShareholderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.chernyavskiy.db.model.Shareholder;

import javax.transaction.Transactional;

@Service
public class ShareholderServiceImpl implements ShareholderService {

    @Autowired
    private StockService stockService;

    @Autowired
    private ShareholderRepository shareholderRepository;

    @Override
    public Shareholder findById(Integer id){
        return shareholderRepository.findById(id).get();
    }

    @Override
    public Iterable<Shareholder> listAll() {
        return shareholderRepository.findAll();
    }

    @Override
    @Transactional
    public void delete(Integer id) {
        stockService.deleteByShareholder(id);
        shareholderRepository.deleteById(id);
    }

    @Override
    public Shareholder add(String firstName, String lastName) {
        return shareholderRepository.save(new Shareholder(firstName, lastName));
    }

    @Override
    public Shareholder edit(Integer id, String firstName, String lastName) {
        Shareholder shareholder = shareholderRepository.findById(id).get();
        shareholder.setFirstName(firstName);
        shareholder.setLastName(lastName);
        return shareholderRepository.save(shareholder);
    }



}
