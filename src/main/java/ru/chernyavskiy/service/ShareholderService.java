package ru.chernyavskiy.service;

import ru.chernyavskiy.db.model.Shareholder;

public interface ShareholderService {

    Shareholder findById(Integer id);

    Iterable<Shareholder> listAll();

    void delete(Integer id);
    
    Shareholder add(String firstName, String lastName);

    Shareholder edit(Integer id, String firstName, String lastName);

}
