package ru.chernyavskiy.service;

import ru.chernyavskiy.db.model.Shareholder;
import ru.chernyavskiy.db.model.Stock;


public interface StockService {

    Iterable<Stock> findByShareholder(Integer shareholder);

    Stock findById(Integer stockId);

    Iterable<Stock> listAll();

    void delete(Integer id);

    void deleteByShareholder(Integer id);

    Stock add(String name, Double start, Double current, int shareholder);

    Stock edit(Integer id, String name, Double start, Double current, int shareholder);
}
