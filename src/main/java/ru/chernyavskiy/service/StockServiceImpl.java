package ru.chernyavskiy.service;

import ru.chernyavskiy.db.dao.StockRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ru.chernyavskiy.db.model.Shareholder;
import ru.chernyavskiy.db.model.Stock;

@Service
public class StockServiceImpl implements StockService {

    @Autowired
    private StockRepository stockRepository;

    @Override
    public Iterable<Stock> findByShareholder(Integer shareholder) {
        return stockRepository.findByShareholder(shareholder);
    }

    @Override
    public Stock findById(Integer stockId) {
        return stockRepository.findById(stockId).get();
    }

    @Override
    public Iterable<Stock> listAll() {
        return stockRepository.findAll();
    }

    @Override
    public void delete(Integer id) {
        stockRepository.delete(stockRepository.findById(id).get());
    }

    @Override
    public void deleteByShareholder(Integer id) {
        stockRepository.deleteByShareholder(id);
    }

    @Override
    public Stock add(String name, Double start, Double current, int shareholder) {
        return stockRepository.save(new Stock(name, start, current, shareholder));
    }

    @Override
    public Stock edit(Integer id, String name, Double start, Double current, int shareholder) {
        Stock stock = stockRepository.findById(id).get();
        stock.setName(name);
        stock.setStart(start);
        stock.setCurrent(current);
        stock.setShareholder(shareholder);
        return stockRepository.save(stock);
    }
}
