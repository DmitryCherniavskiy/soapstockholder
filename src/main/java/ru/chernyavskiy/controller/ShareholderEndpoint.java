package ru.chernyavskiy.controller;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.chernyavskiy.db.model.Shareholder;
import ru.chernyavskiy.service.ShareholderService;
import ru.chernyavskiy.shareholdergen.*;

import java.util.ArrayList;
import java.util.List;

@Endpoint
public class ShareholderEndpoint {

    private static final String NAMESPACE_URI = "http://www.chernyavskiy.ru/shareholderGen";

    private ShareholderService shareholderService;

    @Autowired
    public ShareholderEndpoint(ShareholderService shareholderService) {
        this.shareholderService = shareholderService;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getShareholderByIdRequest")
    @ResponsePayload
    public GetShareholderByIdResponse getShareholderById(@RequestPayload GetShareholderByIdRequest request) {
        GetShareholderByIdResponse response = new GetShareholderByIdResponse();
        Shareholder shareholder = shareholderService.findById(request.getShareholderId());
        ShareholderType shareholderType = new ShareholderType();
        BeanUtils.copyProperties(shareholder, shareholderType);
        response.setShareholderType(shareholderType);
        return response;

    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllShareholdersRequest")
    @ResponsePayload
    public GetAllShareholdersResponse getAllShareholders(@RequestPayload GetAllShareholdersRequest request) {
        GetAllShareholdersResponse response = new GetAllShareholdersResponse();
        List<ShareholderType> shareholderTypeList = new ArrayList<ShareholderType>();
        Iterable<Shareholder> shareholderList = shareholderService.listAll();
        for (Shareholder entity : shareholderList) {
            ShareholderType shareholderType = new ShareholderType();
            BeanUtils.copyProperties(entity, shareholderType);
            shareholderTypeList.add(shareholderType);
        }
        response.getShareholderType().addAll(shareholderTypeList);

        return response;

    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "addShareholderRequest")
    @ResponsePayload
    public AddShareholderResponse addShareholder(@RequestPayload AddShareholderRequest request) {
        AddShareholderResponse response = new AddShareholderResponse();
        ShareholderType newShareholderType = new ShareholderType();
        ServiceStatus serviceStatus = new ServiceStatus();

        Shareholder savedShareholder = shareholderService.add(request.getFirstname(), request.getLastname());

        if (savedShareholder == null) {
            serviceStatus.setStatusCode("CONFLICT");
            serviceStatus.setMessage("Exception while adding Shareholder");
        } else {

            BeanUtils.copyProperties(savedShareholder, newShareholderType);
            serviceStatus.setStatusCode("SUCCESS");
            serviceStatus.setMessage("Content Added Successfully");
        }

        response.setShareholderType(newShareholderType);
        response.setServiceStatus(serviceStatus);
        return response;

    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "updateShareholderRequest")
    @ResponsePayload
    public UpdateShareholderResponse updateShareholder(@RequestPayload UpdateShareholderRequest request) {
        UpdateShareholderResponse response = new UpdateShareholderResponse();
        ServiceStatus serviceStatus = new ServiceStatus();
        Shareholder shareholderFromDB = shareholderService.findById(request.getId());

        if(shareholderFromDB == null) {
            serviceStatus.setStatusCode("NOT FOUND");
            serviceStatus.setMessage("Shareholder = " + request.getId() + " not found");
        }else {

            shareholderFromDB.setFirstName(request.getFirstname());
            shareholderFromDB.setLastName(request.getLastname());

            Shareholder flag = shareholderService.edit(request.getId(), request.getFirstname(), request.getLastname());

            if(flag == null) {
                serviceStatus.setStatusCode("CONFLICT");
                serviceStatus.setMessage("Exception while updating Entity=" + request.getId());;
            }else {
                serviceStatus.setStatusCode("SUCCESS");
                serviceStatus.setMessage("Content updated Successfully");
            }
        }

        response.setServiceStatus(serviceStatus);
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "deleteShareholderRequest")
    @ResponsePayload
    public DeleteShareholderResponse deleteShareholder(@RequestPayload DeleteShareholderRequest request) {
        DeleteShareholderResponse response = new DeleteShareholderResponse();
        ServiceStatus serviceStatus = new ServiceStatus();

        shareholderService.delete(request.getShareholderId());

        if (shareholderService.findById(request.getShareholderId()) != null) {
            serviceStatus.setStatusCode("FAIL");
            serviceStatus.setMessage("Exception while deletint Entity id=" + request.getShareholderId());
        } else {
            serviceStatus.setStatusCode("SUCCESS");
            serviceStatus.setMessage("Content Deleted Successfully");
        }

        response.setServiceStatus(serviceStatus);
        return response;
    }
}


