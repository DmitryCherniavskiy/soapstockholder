package ru.chernyavskiy.controller;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import ru.chernyavskiy.db.model.Stock;
import ru.chernyavskiy.service.StockService;
import ru.chernyavskiy.stockgen.*;

import java.util.ArrayList;
import java.util.List;

@Endpoint
public class StockEndpoint {
    public static final String NAMESPACE_URI = "http://www.chernyavskiy.ru/stockGen";

    private StockService service;

    @Autowired
    public StockEndpoint(StockService service) {
        this.service = service;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getStockByIdRequest")
    @ResponsePayload
    public GetStockByIdResponse getStockById(@RequestPayload GetStockByIdRequest request) {
        GetStockByIdResponse response = new GetStockByIdResponse();
        Stock stockEntity = service.findById(request.getStockId());
        StockType stockType = new StockType();
        BeanUtils.copyProperties(stockEntity, stockType);
        response.setStockType(stockType);
        return response;

    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllStocksRequest")
    @ResponsePayload
    public GetAllStocksResponse getAllStocks(@RequestPayload GetAllStocksRequest request) {
        GetAllStocksResponse response = new GetAllStocksResponse();
        List<StockType> stockTypeList = new ArrayList<StockType>();
        Iterable<Stock> stockEntityList = service.listAll();
        for (Stock entity : stockEntityList) {
            StockType stockType = new StockType();
            BeanUtils.copyProperties(entity, stockType);
            stockTypeList.add(stockType);
        }
        response.getStockType().addAll(stockTypeList);
        return response;

    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "addStockRequest")
    @ResponsePayload
    public AddStockResponse addStock(@RequestPayload AddStockRequest request) {
        AddStockResponse response = new AddStockResponse();
        StockType newStockType = new StockType();
        ServiceStatus serviceStatus = new ServiceStatus();

        Stock savedStock = service.add(request.getName(), request.getStart(), request.getCurrent(), request.getShareholder());

        if (savedStock == null) {
            serviceStatus.setStatusCode("CONFLICT");
            serviceStatus.setMessage("Exception while adding stock");
        } else {
            BeanUtils.copyProperties(savedStock, newStockType);
            serviceStatus.setStatusCode("SUCCESS");
            serviceStatus.setMessage("Stock Added Successfully");
        }

        response.setStockType(newStockType);
        response.setServiceStatus(serviceStatus);
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "updateStockRequest")
    @ResponsePayload
    public UpdateStockResponse updateStock(@RequestPayload UpdateStockRequest request) {
        UpdateStockResponse response = new UpdateStockResponse();
        ServiceStatus serviceStatus = new ServiceStatus();
        Stock stockFromDB = service.findById(request.getId());

        if(stockFromDB == null) {
            serviceStatus.setStatusCode("NOT FOUND");
            serviceStatus.setMessage("Stock = " + request.getId() + " not found");
        }else {

            Stock editStock = service.edit(request.getId(), request.getName(), request.getStart(), request.getCurrent(), request.getShareholder());

            if(editStock == null) {
                serviceStatus.setStatusCode("CONFLICT");
                serviceStatus.setMessage("Exception while updating Entity=" + request.getName());;
            }else {
                serviceStatus.setStatusCode("SUCCESS");
                serviceStatus.setMessage("Content updated Successfully");
            }


        }

        response.setServiceStatus(serviceStatus);
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "deleteStockRequest")
    @ResponsePayload
    public DeleteStockResponse deleteStock(@RequestPayload DeleteStockRequest request) {
        DeleteStockResponse response = new DeleteStockResponse();
        ServiceStatus serviceStatus = new ServiceStatus();

        service.delete(request.getStockId());

        if (service.findById(request.getStockId()) != null) {
            serviceStatus.setStatusCode("FAIL");
            serviceStatus.setMessage("Exception while deletint Entity id=" + request.getStockId());
        } else {
            serviceStatus.setStatusCode("SUCCESS");
            serviceStatus.setMessage("Content Deleted Successfully");
        }

        response.setServiceStatus(serviceStatus);
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getStockByShareholderIdRequest")
    @ResponsePayload
    public GetStockByShareholderIdResponse getStockByShareholderId(@RequestPayload GetStockByShareholderIdRequest request) {
        GetStockByShareholderIdResponse response = new GetStockByShareholderIdResponse();
        List<StockType> stockTypeList = new ArrayList<>();
        Iterable<Stock> stockEntityList = service.findByShareholder(request.getShareholderId());
        for (Stock entity : stockEntityList) {
            StockType stockType = new StockType();
            BeanUtils.copyProperties(entity, stockType);
            stockTypeList.add(stockType);
        }
        response.getStockType().addAll(stockTypeList);
        return response;

    }

}
