# SOAPStockholder

Приложение представляет собой блокнот для брокера, в который он может записывать и редактировать своих клиентов и акции, купленные для них. Приложение реализованно на протоколе SOAP. 

## Getting started
Отличия SOAP подхода от рестового заключается в передаче данных с помощью XML, который генируется автоматически в моем случае из файла stocks.xsd, в котором описаны форматы request и responce запросов для отображения удаления редактирования и т.д.
С этого файла и стоит начать написание. После создания этого файла и описания в нем шаблонов запросов следует добавить webServiceConfig, в котором подключается созданная нами схема.
После конфигурации нужно запустить mvn compile, который сгенерирует классы аналогичные описанным. Сгенерируются они по пути, указанному в теге схемы в xsd файле в моем случае это выглядит так
<xs:schema xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tns="http://www.chernyavskiy.ru/stockGen"
           targetNamespace="http://www.chernyavskiy.ru/stockGen" elementFormDefault="qualified">

После генерации файлов необходимо написать endPoint для ваших запросов.

Затем для тестирования я использовал приложение SOAPUI 
https://www.soapui.org/getting-started/soap-test.html
Вход только с впн.

Для тестирования нужно добавить wsdl файл, который лежит по адресу http://localhost:8080/ws/stocks.wsdl, я просто скопировал его в файл лежащий в проекте. Адрес по которому лежит этот файл указывется в конфигурации.

После добавления этого файла создаются шаблоны запросов, которые можно использовать.

## Useful articles
https://www.javaspringclub.com/publish-and-consume-soap-web-services-using-spring-boot-part-1/
https://www.baeldung.com/spring-boot-soap-web-service


